<?php
$servername = "servername";
$username = "username";
$password = "password";
$db = "dbname";
// Create connection
$conn = new mysqli($servername, $username, $password);
$conn -> select_db($db);
?>

<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="./css/styles.css">
</head>
<body>
<h2>Connection Status</h2>
<table style="width:50%">
<tr>
<th>Connection</th>
<th>Status</th>
</tr>
<tr>
<td>Current Server</td>
<td>
<?php
$hostname = gethostname();
echo $hostname;
?>
</td>
</tr>
<tr>
<td>Database Connection</td>
<td>
<?php
if ($conn->connect_error) {
print("Connection failed: " . $conn->connect_error);
}{
echo "Connected successfully";
}
?>
</td>
</tr>
<tr>
<td>Insert database</td>
<td>
<?php
$sql = "INSERT INTO ".$db".`logging` (`server`) VALUES ('$hostname')";
if ($conn->query($sql) === TRUE) {
echo "New record created successfully";
} else {
echo "Error: " . $sql . "<br>" . $conn->error;
}
?>
</td>
</tr>
</table>
<?php
/* show tables */
$result = $conn->query('SHOW TABLES') or die('cannot show tables');
while($tableName = $result -> fetch_row()) {
$table = $tableName[0];
echo '<h3>Table Name:',$table,'</h3>';
$result2 =$conn->query('SHOW COLUMNS FROM '.$table) or die('cannot show columns from '.$table);
if(mysqli_num_rows($result2)) {
echo '<table cellpadding="0" cellspacing="0" class="db-table">';
echo '<tr><th>Field</th><th>Type</th><th>Null</th><th>Key</th><th>Default<th>Extra</th></tr>';
while($row2 = mysqli_fetch_row($result2)) {
echo '<tr>';
foreach($row2 as $key=>$value) {
echo '<td>',$value,'</td>';
}
echo '</tr>';
}
echo '</table><br />';
}
}
$result = $conn->query('SELECT server,COUNT(server) as count FROM '.$db'.logging GROUP BY server');
echo '<table class="db-table">';
echo "<tr><th>Server</th><th>Number of View</th></tr>";
while($row = $result -> fetch_row()){ //Creates a loop to loop through results
echo "<tr><td>" . $row[0] . "</td><td>" . $row[1] . "</td></tr>"; //$row['index'] the index here is a field name
}
echo "</table>"; //Close the table in HTML
$mysqli -> close();
?>
</body>
</html>


